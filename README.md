# Advent of Code 2021

My Advent of Code solutions for 2021.

- [X] [Day 1](./day-01/solution.py) - fun use of `tee`
- [X] [Day 2](./day-02/solution.py) - `lambda`s in `dict`ionaries.
- [X] [Day 3](./day-03/solution.py) - an exercise in `zip`s and binary partitioning
- [X] [Day 4](./day-04/solution.py) - an OO solution to the problem
- [X] [Day 5](./day-05/solution.py) - nothing to see, here
- [ ] Day 6
- [ ] Day 7
- [ ] Day 8
- [ ] Day 9
- [ ] Day 10
- [ ] Day 11
- [ ] Day 12
- [ ] Day 13
- [ ] Day 14
- [ ] Day 15
- [ ] Day 16
- [ ] Day 17
- [ ] Day 18
- [ ] Day 19
- [ ] Day 20
- [ ] Day 21
- [ ] Day 22
- [ ] Day 23
- [ ] Day 24
- [ ] Day 25
