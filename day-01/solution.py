from itertools import tee


def read_data(data_file, fn):
    with open(data_file) as f:
        return [fn(line) for line in f]


def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def sliding_window_sums(distances):
    return [
        a + b + c for a, b, c in zip(distances, distances[1:], distances[2:])
    ]


def solution(distances):
    count = 0
    for a, b in pairwise(distances):
        if a < b:
            count += 1
    return count


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, int)
        print(solution(data))
        print(solution(sliding_window_sums(data)))
