from copy import copy
from itertools import zip_longest
from typing import Callable

LineSegment = tuple[list[int], list[int]]
Parser = Callable[[int, str], LineSegment]
ProblemInput = list[LineSegment]


def parse(line_no: int, line: str) -> LineSegment:
    start_s, end_s = line.strip().split(" -> ")
    start = [int(x) for x in start_s.split(",")]
    end = [int(x) for x in end_s.split(",")]
    return start, end


def read_data(data_file: str, fn: Parser) -> ProblemInput:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


def solution(data: ProblemInput, has_diagonals: bool = False) -> int:
    max_x = max([max(start[0], end[0]) for start, end in data])
    max_y = max([max(start[1], end[1]) for start, end in data])
    board = [copy([0] * (max_x + 1)) for _ in range(max_y + 1)]
    for start, end in data:
        step_x = 1 if start[0] < end[0] else -1
        step_y = 1 if start[1] < end[1] else -1
        if start[0] == end[0]:
            y_values = range(start[1], end[1] + step_y, step_y)
            values = zip_longest(y_values, [], fillvalue=start[0])
        elif start[1] == end[1]:
            x_values = range(start[0], end[0] + step_x, step_x)
            values = zip_longest([], x_values, fillvalue=start[1])
        elif has_diagonals:
            x_values = range(start[0], end[0] + step_x, step_x)
            y_values = range(start[1], end[1] + step_y, step_y)
            values = zip(y_values, x_values)
        for i, j in values:
            board[i][j] += 1
    return len(
        [
            1
            for i in range(max_y + 1)
            for j in range(max_x + 1)
            if board[i][j] > 1
        ]
    )


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data))
        print(solution(data, has_diagonals=True))
