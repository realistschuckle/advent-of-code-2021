"""
Advent of Code 2021 - Day 3 solution code
"""


def parse(line):
    """Return a string stripped of its trailing newline"""
    value = line.strip()
    return value


def read_data(data_file, fn):
    """Reads data from the specified data file applying
    the provided function to each line.

    Arguments:
    data_file -- the path to the data file
    fn        -- a function to apply to each line of
                 the file"""
    with open(data_file) as f:
        return [fn(line) for line in f]


def counts(data):
    """Counts the number of zeroes and ones in each "bit
    position" of the string entries of data."""
    ones = [sum([int(p) for p in pos]) for pos in zip(*data)]
    zeroes = [len(data) - x for x in ones]
    return zeroes, ones


def diagnostic(entries):
    """Calculates gamma * epsilon"""
    zeroes, ones = counts(entries)
    gamma_string = ["0" if z > o else "1" for z, o in zip(zeroes, ones)]
    gamma = int("".join(gamma_string), base=2)
    epsilon = 2 ** len(entries[0]) - 1 - gamma
    return gamma * epsilon


def life_support(entries):
    """Calculates oxygen generator rating * CO2 scrubber rating."""

    def calc_rating(partition_test):
        """Binary search for the correct element

        This binary search finds the partition between elements
        that have 0 and 1 in each of the k bits. This is faster
        if log(number of entries) < number of "bits" in an entry.

        Arguments:
        partition_test -- determines whether to keep the lower
                          or upper part of the partition"""
        data = list(sorted(entries))
        start = 0
        end = len(entries)
        for bit in range(len(data[0])):
            lo = start
            hi = end
            while lo <= hi:
                mid = (hi + lo) // 2
                if data[mid][bit] == data[start][bit]:
                    lo = mid + 1
                else:
                    hi = mid - 1
            if partition_test(lo - start, end - hi - 1):
                start = lo
            else:
                end = hi + 1
            if end - start == 1:
                break
        return int(data[start], base=2)

    o2 = calc_rating(lambda lsize, rsize: rsize >= lsize)
    co2 = calc_rating(lambda lsize, rsize: rsize < lsize)
    return o2 * co2


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(diagnostic(data))
        print(life_support(data))
