"""
Advent of Code 2021 - Day 4 solution code
"""
from collections.abc import Callable
from copy import copy
from itertools import groupby, repeat
import re
from typing import Union

LineParser = Callable[int, str, list[int]]
ProblemInput = dict[str, Union[list[int], list[list[list[int]]]]]


def parse(line_no: int, line: str) -> list[int]:
    """Parses the given line into a list of integers.

    If this is the first line (line_no == 0), then it
    splits using a comma-delimited list. Otherwise, for
    all non-empty lines, it splits on a space-delimited
    list.

    Arguments:
    line_no -- the zero-based index of the line in the
               data set
    line    -- the string to parse"""
    if line_no == 0:
        return [int(x) for x in line.strip().split(",")]
    if line.strip():
        return [int(x) for x in re.split(r"\s+", line.strip())]


def read_data(data_file: str, fn: LineParser) -> ProblemInput:
    """Reads data from the specified data file applying
    the provided function to each line.

    Arguments:
    data_file -- the path to the data file
    fn        -- a function to apply to each line of
                 the file

    Return value:
    Returns a dictionary with the following keys:
    draws  -- the list of integers that are the draws
    boards -- a list of 5x5 lists that represent the
              boards to be played
    """
    with open(data_file) as f:
        dirty_data = [fn(i, line) for i, line in enumerate(f)]
    data = [line for line in dirty_data if line]
    boards = [
        list(y) for x, y in groupby(enumerate(data[1:]), lambda v: v[0] // 5)
    ]
    for i, board in enumerate(boards):
        boards[i] = [y for x, y in board]
    return {"draws": data[0], "boards": boards}


class Board:
    def __init__(self, values: list[list[int]]):
        """Create a Board with the supplied values"""
        self.values = values
        self.marks = [copy(list(repeat(False, 5))) for _ in values]
        self._is_winner = False

    def mark(self, draw):
        """Mark the board with the given draw if it is
        not alread a winner."""
        if self._is_winner:
            return
        for i, row in enumerate(self.values):
            for j, column in enumerate(row):
                if column == draw:
                    self.marks[i][j] = True
        if self.is_winner:
            self.winning_draw = draw

    @property
    def is_winner(self):
        """Determine if the board has a row or column
        fully marked."""
        if self._is_winner:
            return True
        for row in self.marks:
            if all(row):
                self._is_winner = True
        for column in zip(*self.marks):
            if all(column):
                self._is_winner = True
        return self._is_winner

    @property
    def score(self):
        """Get the score of the board from the product of
        the sum of unmarked locations and the value of the
        draw that caused the board to win."""
        if not self.is_winner:
            return None
        sum = 0
        for i, row in enumerate(self.marks):
            for j, column in enumerate(row):
                if not column:
                    sum += self.values[i][j]
        return self.winning_draw * sum


class BoardCollection:
    def __init__(self, boards):
        """Create a collection of boards."""
        self.boards = boards
        self.winners = [0 for _ in boards]
        self.next_winner = 1

    def mark(self, draw):
        """Mark all of the boards in the collection with the
        provided draw."""
        for i, board in enumerate(self.boards):
            board.mark(draw)
            if not self.winners[i] and board.is_winner:
                self.winners[i] = self.next_winner
                self.next_winner += 1

    @property
    def has_all_winners(self):
        """Determines if all boards in the collection are in
        a winning state."""
        return all(self.winners)

    @property
    def winning_score(self):
        """Get the score of the board that wins first in the
        collection."""
        return self._find_board_score(1)

    @property
    def losing_score(self):
        """Get the score of the board that wins last in the
        collection."""
        key = len(self.boards)
        return self._find_board_score(key)

    def _find_board_score(self, key):
        index = [i for i, v in enumerate(self.winners) if v == key][0]
        return self.boards[index].score


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        draws = data["draws"]
        board_list = [Board(values) for values in data["boards"]]
        boards = BoardCollection(board_list)

        for draw in draws:
            boards.mark(draw)
            if boards.has_all_winners:
                print(boards.winning_score)
                print(boards.losing_score)
                break
